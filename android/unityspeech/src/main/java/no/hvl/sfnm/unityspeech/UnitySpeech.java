package no.hvl.sfnm.unityspeech;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;

import com.unity3d.player.UnityPlayer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class UnitySpeech {

    private SpeechRecognizer speechRecognizer;
    private Intent recognizerIntent;
    private AudioManager audioManager;
    private Activity activity;
    private String unityObjectName;

    public UnitySpeech(final Activity activity, String unityObjectName){
        this.activity = activity;
        this.unityObjectName = unityObjectName;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                speechRecognizer = SpeechRecognizer.createSpeechRecognizer(activity);
                recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                speechRecognizer.setRecognitionListener(recognitionListener);
                audioManager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
            }
        });

    }

    public void start(){
        Log.d("UnitySpeech", "start");
        System.out.println("start");
        //mute();
        activity.runOnUiThread(new Runnable(){
            @Override
            public void run() {
                speechRecognizer.startListening(recognizerIntent);
            }
        });
    }

    public void stop(){
        Log.d("UnitySpeech", "stop");
        //unMute();
        activity.runOnUiThread(new Runnable(){
            @Override
            public void run() {
                speechRecognizer.stopListening();
            }
        });
    }

    private RecognitionListener recognitionListener = new RecognitionListener() {
        @Override
        public void onReadyForSpeech(Bundle params) {
            Log.d("UnitySpeech", "onReadyForSpeech");
        }

        @Override
        public void onBeginningOfSpeech() {
            Log.d("UnitySpeech", "onBeginningOfSpeech");
        }

        @Override
        public void onRmsChanged(float rmsdB) {
        }

        @Override
        public void onBufferReceived(byte[] buffer) {
            Log.d("UnitySpeech", "onBufferReceived");
        }

        @Override
        public void onEndOfSpeech() {
            Log.d("UnitySpeech", "onEndOfSpeech");
        }

        @Override
        public void onError(int error) {
            Log.d("UnitySpeech", "onError: " + error);
            //unMute();
            String errorString = "unknown error";
            switch(error){
                case 3: errorString = "ERROR_AUDIO"; break;
                case 5: errorString = "ERROR_CLIENT"; break;
                case 9: errorString = "ERROR_INSUFFICIENT_PERMISSIONS"; break;
                case 2: errorString = "ERROR_NETWORK"; break;
                case 1: errorString = "ERROR_NETWORK_TIMEOUT"; break;
                case 7: errorString = "ERROR_NO_MATCH"; break;
                case 8: errorString = "ERROR_RECOGNIZER_BUSY"; break;
                case 4: errorString = "ERROR_SERVER"; break;
                case 6: errorString = "ERROR_SPEECH_TIMEOUT"; break;
            }
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("code", error);
                jsonObject.put("name", errorString);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace();
            }
            UnityPlayer.UnitySendMessage(unityObjectName, "OnError", jsonObject.toString());
        }

        @Override
        public void onResults(Bundle results) {
            Log.d("UnitySpeech", "onResults: " + results);
            //unMute();
            List<String> strings = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            JSONArray jsonArray = new JSONArray();
            for(String string : strings){
                jsonArray.put(string);
            }
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("results", jsonArray);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace();
            }
            UnityPlayer.UnitySendMessage(unityObjectName, "OnResult", jsonObject.toString());
        }

        @Override
        public void onPartialResults(Bundle partialResults) {
            Log.d("UnitySpeech", "onPartialResults: " + partialResults);
        }

        @Override
        public void onEvent(int eventType, Bundle params) {
            Log.d("UnitySpeech", "onEvent eventType: " + eventType +  " params: " + params);
        }
    };

    /*private void mute(){
        audioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
    }

    private void unMute(){
        activity.runOnUiThread(new Runnable(){
            @Override
            public void run() {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        audioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
                    }
                }, 1000);
            }
        });

    }*/

}