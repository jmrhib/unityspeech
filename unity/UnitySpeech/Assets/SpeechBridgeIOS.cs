﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class SpeechBridgeIOS : ISpeechBridge {

	[DllImport("__Internal")]
	private static extern void setGameObjectName (string name);
	[DllImport("__Internal")]
	private static extern void startSpeech ();
	[DllImport("__Internal")]
	private static extern void stopSpeech ();

	public SpeechBridgeIOS(string name){
		setGameObjectName (name);
	}

	public void StartSpeech(){
		startSpeech ();
	}

	public void StopSpeech(){
		startSpeech ();
	}
}
