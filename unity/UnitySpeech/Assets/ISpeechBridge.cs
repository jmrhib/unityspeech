﻿public interface ISpeechBridge {

	void StartSpeech();
	void StopSpeech();

}
