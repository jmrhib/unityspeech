﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeechBridgeAndroid : ISpeechBridge {

	private AndroidJavaObject androidObject;

	public SpeechBridgeAndroid(string objectName){
		AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject unityActivity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
		object[] parameters = new object[2];
		parameters [0] = unityActivity;
		parameters [1] = objectName;
		androidObject = new AndroidJavaObject ("no.hvl.sfnm.unityspeech.UnitySpeech", parameters);
	}

	public void StartSpeech(){
		Debug.Log ("StartSpeech");
		androidObject.Call ("start");
	}

	public void StopSpeech(){
		androidObject.Call ("stop");
	}
}
