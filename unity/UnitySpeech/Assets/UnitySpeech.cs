﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitySpeech : MonoBehaviour {

	private ISpeechBridge speechBridge;
	public Text output;

	public void Start(){
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			speechBridge = new SpeechBridgeIOS (this.name);
		}
		if (Application.platform == RuntimePlatform.Android) {
			speechBridge = new SpeechBridgeAndroid (this.name);
		}

		output.text = "Start";
	}

	public void Stop(){
		StopSpeech ();
	}

	public void StartSpeech(){
		speechBridge.StartSpeech ();
	}

	public void StopSpeech(){
		speechBridge.StopSpeech ();
	}

	private void OnResult(string resultsJSON){
		Debug.Log (resultsJSON);
		string[] results = JsonUtility.FromJson<SpeechResult>(resultsJSON).results;
		output.text = results[0];
	}

	private void OnError(string errorJSON){
		Debug.Log (errorJSON);
		SpeechError speechError = JsonUtility.FromJson<SpeechError>(errorJSON);
		output.text = speechError.name;
	}

	private class SpeechResult {
		public string[] results;
	}

	private class SpeechError {
		public int code;
		public string name;
	}
}
