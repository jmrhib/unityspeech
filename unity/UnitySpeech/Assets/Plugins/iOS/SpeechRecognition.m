//
//  SpeechRecognition.m
//
//
//  Created by Johannes Mario Ringheim on 09/02/2019.
//

#import "SpeechRecognition.h"

@implementation SpeechRecognition

- (instancetype)init
{
    return [self initWithGameObjectName:@"none"];
}

-(instancetype)initWithGameObjectName: (NSString *) gameObjectName {
    self = [super init];
    if(self){
        
        self.gameObjectName = gameObjectName;
        
        speechRecognizer = [[SFSpeechRecognizer alloc] initWithLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"no"]];
        speechRecognizer.delegate = self;
        
        [SFSpeechRecognizer requestAuthorization:^(SFSpeechRecognizerAuthorizationStatus status) {
            NSLog(@"status %ld", (long)status);
            switch (status) {
                case SFSpeechRecognizerAuthorizationStatusAuthorized:
                    NSLog(@"requestAuthorization Authorized");
                    break;
                case SFSpeechRecognizerAuthorizationStatusDenied:
                    NSLog(@"requestAuthorization Denied");
                    break;
                case SFSpeechRecognizerAuthorizationStatusNotDetermined:
                    NSLog(@"requestAuthorization Not Determined");
                    break;
                case SFSpeechRecognizerAuthorizationStatusRestricted:
                    NSLog(@"requestAuthorization Restricted");
                    break;
                default:
                    NSLog(@"requestAuthorization default");
                    break;
            }
        }];
    }
    return self;
}

-(void) startSpeech {
    
    NSLog(@"startSpeech from object");
    NSLog(@"%@",[NSThread callStackSymbols]);
    
    if(audioEngine != nil){
        NSLog(@"audioEngine nonnull");
        return;
    }
    
    if (recognitionTask) {
        [recognitionTask cancel];
        recognitionTask = nil;
    }
    
    audioEngine = [[AVAudioEngine alloc] init];
    
    // Starts an AVAudio Session
    NSError *error;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    [audioSession setActive:YES withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:&error];
    
    recognitionRequest = [[SFSpeechAudioBufferRecognitionRequest alloc] init];
    inputNode = audioEngine.inputNode;
    recognitionRequest.shouldReportPartialResults = YES;
    recognitionTask = [speechRecognizer recognitionTaskWithRequest:recognitionRequest resultHandler:^(SFSpeechRecognitionResult * _Nullable result, NSError * _Nullable error) {
        BOOL isFinal = NO;
        if (result) {
            finalResult = result.bestTranscription.formattedString;
            if(result.isFinal){
                NSLog(@"FINAL RESULT:%@", finalResult);
                [self stopSpeech];
                [self sendResult];
            }
            else {
                [self resetTimer];
                NSLog(@"RESULT:%@", finalResult);
            }
            isFinal = !result.isFinal;
        }
        if (error) {
            NSLog(@"ERROR:%@", error);
            [self stopSpeech];
            [self sendError: error];
        }
    }];
    
    AVAudioFormat *recordingFormat = [inputNode outputFormatForBus:0];
    [inputNode installTapOnBus:0 bufferSize:1024 format:recordingFormat block:^(AVAudioPCMBuffer * _Nonnull buffer, AVAudioTime * _Nonnull when) {
        [recognitionRequest appendAudioPCMBuffer:buffer];
    }];
    
    // Starts the audio engine, i.e. it starts listening.
    [audioEngine prepare];
    [audioEngine startAndReturnError:&error];
    NSLog(@"AUDIOENGINE ERROR:%@", [error localizedDescription]);
    NSLog(@"Say Something, I'm listening");
}

-(void) sendError: (NSError *) error {
    NSMutableString *errorJson = [[NSMutableString alloc] init];
    [errorJson appendString: @"{\"name\": \""];
    [errorJson appendString: [error localizedDescription]];
    [errorJson appendString: @"\", \"code\":"];
    [errorJson appendString: [NSString stringWithFormat:@"%ld", [error code]]];
    [errorJson appendString: @"}"];
    UnitySendMessage([self.gameObjectName UTF8String], "OnError", [errorJson UTF8String]);
}

-(void) sendResult {
    NSMutableString *resultsJson = [[NSMutableString alloc] init];
    [resultsJson appendString: @"{\"results\": [\""];
    [resultsJson appendString: finalResult];
    [resultsJson appendString: @"\"]}"];
    UnitySendMessage([self.gameObjectName UTF8String], "OnResult", [resultsJson UTF8String]);
}

-(void) stopSpeech {
    //if (audioEngine.isRunning) {
    [audioEngine.inputNode removeTapOnBus:0];
    [audioEngine stop];
    [recognitionRequest endAudio];
    audioEngine = nil;
    recognitionRequest = nil;
    recognitionTask = nil;
    //}
    if(timer){
        [timer invalidate];
    }
    NSLog(@"stopSpeech from object");
}

-(void)timerCalled {
    NSLog(@"Timer Called");
    [self stopSpeech];
    //[self sendResult];
}

-(void)resetTimer {
    if(timer){
        [timer invalidate];
    }
    timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(timerCalled) userInfo:nil repeats:NO];
}

- (void)speechRecognizer:(SFSpeechRecognizer *)speechRecognizer availabilityDidChange:(BOOL)available {
    NSLog(@"Availability:%d",available);
}

@end
