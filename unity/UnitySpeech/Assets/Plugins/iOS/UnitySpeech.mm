#import <Foundation/Foundation.h>
#import "SpeechRecognition.h"

@interface UnitySpeech : NSObject {
}
@end

@implementation UnitySpeech

static UnitySpeech* sharedInstance;

SpeechRecognition *speechRecognition = [[SpeechRecognition alloc] init];

+(UnitySpeech*) sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[UnitySpeech alloc] init];
    });
    return sharedInstance;
}

-(id) init {
    self = [super init];
    return self;
}

-(void) setGameObjectName: (NSString*) name {
    [speechRecognition setGameObjectName: name];
}

-(void) startSpeech {
    [speechRecognition startSpeech];
}

-(void) stopSpeech {
    [speechRecognition stopSpeech];
}

@end

extern "C" {
    
    void startSpeech(){
        [[UnitySpeech sharedInstance] startSpeech];
    }
    
    void stopSpeech(){
        [[UnitySpeech sharedInstance] stopSpeech];
    }
    
    void setGameObjectName(char* name){
        [[UnitySpeech sharedInstance] setGameObjectName: [NSString stringWithUTF8String: name]];
    }
}
