//
//  SpeechRecognition.h
//
//
//  Created by Johannes Mario Ringheim on 09/02/2019.
//

#import <Foundation/Foundation.h>
#import <Speech/Speech.h>

NS_ASSUME_NONNULL_BEGIN

@interface SpeechRecognition : NSObject <SFSpeechRecognizerDelegate> {
    NSTimer *timer;
    SFSpeechRecognizer *speechRecognizer;
    AVAudioEngine *audioEngine;
    SFSpeechRecognitionTask *recognitionTask;
    SFSpeechAudioBufferRecognitionRequest *recognitionRequest;
    AVAudioInputNode *inputNode;
    NSString *finalResult;
}

@property NSString *gameObjectName;

-(void) startSpeech;

-(void) stopSpeech;

@end

NS_ASSUME_NONNULL_END
